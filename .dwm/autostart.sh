#!/bin/sh

XDG_CONFIG_HOME="$HOME/.config"
export XDG_CONFIG_HOME

setxkbmap ch
sh ~/.dwm/dwm_scripts/screenres.sh

transmission-daemon &

nextcloud &

clipmenud&

compton --backend xrender &

feh --bg-scale /home/dennys/Pictures/Wallpapers/archlabs2.jpg

sh ~/.dwm/dwm_scripts/xsetloop.sh &

# picom --config ~/.config/picom/picom.conf&

