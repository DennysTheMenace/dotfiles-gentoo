/* See LICENSE file for copyright and licenseuddetails. */

/* appearance */
static const unsigned int borderpx = 1; /* border pixel of windows */
static const unsigned int snap = 32;    /* snap pixel */
static const int swallowfloating =
    0;                        /* 1 means swallow floating windows by default */
static const int showbar = 1; /* 0 means no bar */
static const int topbar = 1;  /* 0 means bottom bar */
static const int statmonval = 0;
static const char *fonts[] = {"Spacemono:size=10"};
static const char dmenufont[] = "Spacemono:size=10";
static const char col_gray1[] = "#1c2023";
static const char col_gray2[] = "#444444";
static const char col_gray3[] = "#e1e1e1";
static const char col_gray4[] = "#222b2e";
static const char col_cyan[] = "#96b5b4";
static const char *colors[][3] = {
    /*               fg         bg         border   */
    [SchemeNorm] = {col_gray3, col_gray1, col_gray2},
    [SchemeSel] = {col_gray1, col_cyan, col_cyan},
    [SchemeTitle] = {col_gray3, col_gray1, col_cyan},
};

/* tagging */
static const char *tags[] = {"I",  "II",  "III",  "IV", "V",
                             "VI", "VII", "VIII", "IV"};

static const Rule rules[] = {
    /* xprop(1):
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING) = title
     */
    /* class     instance  title           tags mask  iscentered isfloating
       isterminal  noswallow  monitor */
    {"Gimp", NULL, NULL, 0, 0, 1, 0, 0, -1},
    {"Firefox", NULL, NULL, 1 << 8, 0, 0, 0, -1, -1},
    {"st", NULL, NULL, 0, 0, 0, 1, -1, -1},
    {NULL, NULL, "Event Tester", 0, 0, 1, 0, 1, -1}, /* xev */
};

/* layout(s) */
static const float mfact = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster = 1;    /* number of clients in master area */
static const int resizehints =
    1; /* 1 means respect size hints in tiled resizals */

#include "fibonacci.c"
#include "grid.c"
static const Layout layouts[] = {
    /* symbol               function */
    {"[T]", tile}, /* first entry is default */
    {"[F]", NULL},
    {"[M]", monocle},
    {"[@]", spiral},
    {"[\\]", dwindle},
    {"HHH", grid},
    {"|M|", centeredmaster},
    {">M>", centeredfloatingmaster},
    {NULL, NULL},
};
/* key definitions */
#define WINKEY Mod1Mask
#define MODKEY Mod4Mask
#define TAGKEYS(KEY, TAG)                                                    \
    {MODKEY, KEY, view, {.ui = 1 << TAG}},                                   \
        {MODKEY | ControlMask, KEY, toggleview, {.ui = 1 << TAG}},           \
        {MODKEY | WINKEY, KEY, nview, {.ui = 1 << TAG}},                     \
        {MODKEY | WINKEY | ControlMask, KEY, ntoggleview, {.ui = 1 << TAG}}, \
        {MODKEY | ShiftMask, KEY, tag, {.ui = 1 << TAG}},                    \
        {MODKEY | ControlMask | ShiftMask, KEY, toggletag, {.ui = 1 << TAG}},

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd)                                           \
    {                                                        \
        .v = (const char *[]) { "/bin/sh", "-c", cmd, NULL } \
    }

#include <X11/XF86keysym.h>

#include "movestack.c"

/* commands */
static char dmenumon[2] =
    "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = {
    "dmenu_run", "-m",      dmenumon, "-fn",    dmenufont, "-nb",     col_gray1,
    "-nf",       col_gray3, "-sb",    col_cyan, "-sf",     col_gray1, NULL};
static const char *termcmd[] = {"tabbed", "-c", "-r", "2", "st", "-w", "''", NULL};

static Key keys[] = {
    /* modifier                     key        function        argument */
    {MODKEY, XK_p, spawn, {.v = dmenucmd}},
    {MODKEY | ShiftMask, XK_Return, spawn, {.v = termcmd}},
    {MODKEY, XK_w, spawn, SHCMD("firefox")},
    {MODKEY, XK_f, spawn, SHCMD("pcmanfm")},
    {MODKEY, XK_x, spawn, SHCMD("poweroff-dmenu.sh")},
    {MODKEY, XK_a, spawn, SHCMD("st -e pamix")},
    {MODKEY, XK_s, spawn, SHCMD("st -e ncspot")},
    {MODKEY, XK_m, spawn, SHCMD("protonmail-bridge;st -e neomutt")},
    {MODKEY, XK_v, spawn, SHCMD("clipmenu")},
    {MODKEY, XK_Escape, spawn, SHCMD("gnome-system-monitor")},
    {MODKEY, XK_c, spawn, SHCMD("colorpicker.sh")},
    {MODKEY | ShiftMask, XK_b, spawn, SHCMD("blueman-manager")},
    {MODKEY | ShiftMask, XK_n, spawn, SHCMD("networkmanager_dmenu")},
    {MODKEY, XK_n, spawn, SHCMD("st -e nvim -c VimwikiIndex")},
    {MODKEY | ShiftMask, XK_f, spawn, SHCMD("fmenu")},
    {MODKEY | ShiftMask, XK_z, spawn, SHCMD("tabbed -c zathura -e")},
    {MODKEY | ShiftMask, XK_t, spawn, SHCMD("torrent_scripts.sh -s")},
    {MODKEY | ShiftMask, XK_x, spawn,
     SHCMD("~/.dwm/dwm_scripts/homeofficeres.sh")},
    {MODKEY | ShiftMask, XK_s, spawn,
     SHCMD("flameshot gui")},
    {MODKEY | ShiftMask, XK_p, spawn,
     SHCMD("scrot -e 'mv $f ~/Pictures/screenshot'")},
    {MODKEY, XK_z, togglesticky, {0}},
    {MODKEY, XK_b, togglebar, {0}},
    {MODKEY, XK_j, focusstack, {.i = +1}},
    {MODKEY, XK_k, focusstack, {.i = -1}},
    {MODKEY | ShiftMask, XK_j, movestack, {.i = +1}},
    {MODKEY | ShiftMask, XK_k, movestack, {.i = -1}},
    {MODKEY, XK_i, incnmaster, {.i = +1}},
    {MODKEY, XK_d, incnmaster, {.i = -1}},
    {MODKEY, XK_h, setmfact, {.f = -0.05}},
    {MODKEY, XK_l, setmfact, {.f = +0.05}},
    {MODKEY, XK_Return, zoom, {0}},
    {MODKEY, XK_Tab, view, {0}},
    {MODKEY, XK_q, killclient, {0}},
    {MODKEY, XK_t, setlayout, {.v = &layouts[0]}},
    {MODKEY | ShiftMask, XK_m, setlayout, {.v = &layouts[2]}},
    {MODKEY | ControlMask, XK_comma, cyclelayout, {.i = -1}},
    {MODKEY | ShiftMask, XK_Tab, cyclelayout, {.i = -1}},
    {MODKEY | ControlMask, XK_period, cyclelayout, {.i = +1}},
    {MODKEY, XK_space, setlayout, {0}},
    {MODKEY | ShiftMask, XK_space, togglefloating, {0}},
    {MODKEY, XK_0, view, {.ui = ~0}},
    {MODKEY | ShiftMask, XK_0, tag, {.ui = ~0}},
    {MODKEY, XK_comma, focusmon, {.i = -1}},
    {MODKEY, XK_period, focusmon, {.i = +1}},
    {MODKEY | ShiftMask, XK_comma, tagmon, {.i = -1}},
    {MODKEY | ShiftMask, XK_period, tagmon, {.i = +1}},
    TAGKEYS(XK_1, 0) TAGKEYS(XK_2, 1) TAGKEYS(XK_3, 2) TAGKEYS(XK_4, 3)
        TAGKEYS(XK_5, 4) TAGKEYS(XK_6, 5) TAGKEYS(XK_7, 6) TAGKEYS(XK_8, 7)
            TAGKEYS(XK_9, 8){MODKEY | ShiftMask, XK_c, quit, {0}},
    {MODKEY | ShiftMask, XK_r, quit, {1}},

    {0, XF86XK_AudioMute, spawn, SHCMD("pamixer -t")},
    {0, XF86XK_AudioRaiseVolume, spawn, SHCMD("pamixer -i 2")},
    {0, XF86XK_AudioLowerVolume, spawn, SHCMD("pamixer -d 2")},
    {0, XF86XK_AudioPrev, spawn, SHCMD("playerctl previous")},
    {0, XF86XK_AudioNext, spawn, SHCMD("playerctl next")},
    {0, XF86XK_AudioPlay, spawn, SHCMD("playerctl play-pause")},
    {0, XF86XK_AudioStop, spawn, SHCMD("playerctl stop")},
    {0, XF86XK_MonBrightnessUp, spawn, SHCMD("xbacklight -inc 10")},
    {0, XF86XK_MonBrightnessDown, spawn, SHCMD("xbacklight -dec 10")},
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle,
 * ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function argument
     */
    {ClkLtSymbol, 0, Button1, setlayout, {0}},
    {ClkLtSymbol, 0, Button3, setlayout, {.v = &layouts[2]}},
    {ClkWinTitle, 0, Button2, zoom, {0}},
    {ClkStatusText, 0, Button2, spawn, {.v = termcmd}},
    {ClkClientWin, MODKEY, Button1, movemouse, {0}},
    {ClkClientWin, MODKEY, Button2, togglefloating, {0}},
    {ClkClientWin, MODKEY, Button3, resizemouse, {0}},
    {ClkTagBar, 0, Button1, view, {0}},
    {ClkTagBar, 0, Button3, toggleview, {0}},
    {ClkTagBar, MODKEY | WINKEY, Button1, nview, {0}},
    {ClkTagBar, MODKEY | WINKEY, Button3, ntoggleview, {0}},
    {ClkTagBar, MODKEY, Button1, tag, {0}},
    {ClkTagBar, MODKEY, Button3, toggletag, {0}},
};

