#!/usr/bin/env bash
SEP1=""
SEP2="  "

dwm_wpa () {
    CONNAME=$(iw dev | awk '/ssid/ {print substr($2,index($2,$5))}')
    PUBLIC=$(curl -s https://ipinfo.io/ip)

    if [ "$CONNAME" = "" ]; then
        echo ""
    else
    printf "%s" "$SEP1"
        printf "NET: %s %s" "$CONNAME" "$PUBLIC"
    printf "%s\n" "$SEP2"
    fi
}
dwm_date () {
    printf "%s" "$SEP1"
         printf "DAT: %s" "$(date "+%H:%M, %d.%m.%y ")"
}
dwm_battery () {
    CHARGE=$(cat /sys/class/power_supply/BAT0/capacity)
    STATUS=$(cat /sys/class/power_supply/BAT0/status)

    printf "%s" "$SEP1"
    if [[ $STATUS != "Discharging" ]]; then
        printf "BAT: %s %s" "$CHARGE" "⚡"
    else
        printf "BAT: %s%%" "$CHARGE"
    fi
    printf "%s\n" "$SEP2"
}
dwm_spotify () {
    if ps -C ncspot > /dev/null; then
        PLAYER="ncspot"
    fi

    if ps -C spotify > /dev/null; then
        PLAYER="spotify"
    fi

    if [ "$PLAYER" = "spotify" ] || [ "$PLAYER" = "ncspot" ]; then
        ARTIST=$(playerctl metadata artist)
        TRACK=$(playerctl metadata title)
        POSITION=$(playerctl position | sed 's/..\{6\}$//')
        DURATION=$(playerctl metadata mpris:length | sed 's/.\{6\}$//')
        STATUS=$(playerctl status)
        SHUFFLE=$(playerctl shuffle)

            if [ "$STATUS" = "Playing" ]; then
                STATUS="PLA:"
            else
                STATUS="PAU:"
            fi

            printf "%s%s %s - %s" "$SEP1" "$STATUS" "$ARTIST" "$TRACK"
            printf "%s\n" "$SEP2"
    fi
}
dwm_blth () {
    blthON=$(rc-status | awk '/bluetooth/ {print $3}')
    if [[ blthON = "started" ]]; then
    device=$(bluetoothctl info | grep "Name:" | awk '{print substr($2,index($2,$5))}')

    if [[ "$device" = "" ]]; then
       echo "" 
   else
        printf "%s" "$SEP1"
        printf "BLTH: %s" "$device"
        printf "%s\n" "$SEP2"
    fi
    fi
}
dwm_alsa () {
    VOL=$(amixer sget Master | grep 'Left:' | awk -F'[][]' '{ print $2 }')
    MUTED=$(pacmd list-sinks | awk '/muted/ { print $2;exit }')
    
    printf "%s" "$SEP1"
        if [ "$MUTED" != 'no' ] 
        then
            printf "MUTE"
        else
            printf "VOL: %s" "$VOL"
        fi
    printf "%s\n" "$SEP2"
}


 while true
do
    xsetroot -name "$(dwm_spotify)$(dwm_blth)$(dwm_wpa)$(dwm_battery)$(dwm_alsa)$(dwm_date)"
    sleep 1
done   
