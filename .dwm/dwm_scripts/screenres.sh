#!/usr/bin/env bash

if xrandr | grep "HDMI1 connected"
then
        xrandr --output HDMI1 --mode 2560x1440 --output eDP1 --off --output DP2 --mode 2560x1440 --primary --left-of HDMI1
else
        xrandr --output eDP1 --mode 1920x1080 --primary 
fi
