#!/usr/bin/env bash

RET=$(echo -e "Single\nHomeOffice\nDual\nTriple" | dmenu -p "Screen Setup:")

case $RET in
    *Single)  xrandr --output DP2 --mode 2560x1440 --primary --output eDP1 --off --output HDMI1 --off ;;
		*HomeOffice)  xrandr --output HDMI1 --mode 2560x1440 --primary --output eDP1 --off --output DP2 --off ;;
		*Dual) xrandr --output DP2 --mode 2560x1440 --primary --output eDP1 --off --output HDMI1 --mode 2560x1440 --right-of DP2 ;;
		*Triple) xrandr --output DP2 --mode 2560x1440 --primary --output eDP1 --mode 2560x1440 --left-of DP2 --output HDMI1 --mode 2560x1440 --right-of DP2;;
	*) ;;
esac




