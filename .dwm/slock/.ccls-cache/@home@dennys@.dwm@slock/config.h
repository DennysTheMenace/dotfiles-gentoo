/* user and group to drop privileges to */
static const char *user  = "dennys";
static const char *group = "dennys";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "#1c2023",     /* after initialization */
	[INPUT] =  "#96b5b4",   /* during input */
	[FAILED] = "#bf616a",   /* wrong password */
	[CAPS] =    "#ebcb8b",         /* CapsLock on */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/* time in seconds before the monitor shuts down */
static const int monitortime = 8;

/* default message */
static const char * message = "Suckless: Software that sucks less.";

/* text color */
static const char * text_color = "#e1e1e1";

/* text size (must be a valid size) */
static const char * font_name = "6x13";
