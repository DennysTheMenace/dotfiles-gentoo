
#███████╗███████╗██╗  ██╗██████╗  ██████╗
#╚══███╔╝██╔════╝██║  ██║██╔══██╗██╔════╝
#  ███╔╝ ███████╗███████║██████╔╝██║     
# ███╔╝  ╚════██║██╔══██║██╔══██╗██║     
#███████╗███████║██║  ██║██║  ██║╚██████╗
#╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝

source ~/.zplug/init.zsh

#Plugins
zplug "zplug/zplug", hook-build:"zplug --self-manage"
zplug "softmoth/zsh-vim-mode"
zplug "plugins/colored-man-pages",   from:oh-my-zsh
zplug "plugins/command-not-found",   from:oh-my-zsh
zplug "MichaelAquilina/zsh-auto-notify"
zplug "ajeetdsouza/zoxide"
zplug "pabloariasal/zfm"

#Completion and Highlighting
zplug "zsh-users/zsh-completions",              defer:0
zplug "zsh-users/zsh-autosuggestions",          defer:2, on:"zsh-users/zsh-completions"
zplug "zsh-users/zsh-syntax-highlighting",      defer:3, on:"zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-history-substring-search", defer:3, on:"zsh-users/zsh-syntax-highlighting"

#fzf
zplug 'Aloxaf/fzf-tab'

# Load theme file
zplug "denysdovhan/spaceship-prompt", use:spaceship.zsh, from:github, as:theme

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# source plugins and add commands to $PATH
zplug load

#history
HISTFILE=~/.zsh/zsh_history
HISTSIZE=100000
SAVEHIST=100000
setopt appendhistory

# Personal configuration

#theme settings
source ~/.zsh/settings/theme

# aliases
source ~/.zsh/settings/aliases

#Export variables
source ~/.zsh/settings/export

#Plugins settings
source ~/.zsh/settings/plugins

#functions
source ~/.zsh/settings/functions

#autostart at startup
