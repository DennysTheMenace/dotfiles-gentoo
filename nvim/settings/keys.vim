"Keybindings

"Leader key
let mapleader = "\<space>"

"Buffer
nnoremap <silent> <leader>1 :b1<cr>
nnoremap <silent> <leader>2 :b2<cr>
nnoremap <silent> <leader>3 :b3<cr>
nnoremap <silent> <leader>4 :b4<cr>
nnoremap <silent> <leader>5 :b5<cr>
nnoremap <silent> <leader>6 :b6<cr>
nnoremap <silent> <leader>7 :b7<cr>
nnoremap <silent> <leader>8 :b8<cr>
nnoremap <silent> <leader>9 :b9<cr>
nnoremap <silent> <leader>n :bn<cr>
nnoremap <silent> <leader>b :bp<cr>
nnoremap <silent> <leader>d :bd<cr>
nnoremap <leader>bs :b<CR>:buffer<Space>

"splitkeys
nnoremap <leader>j <C-W><C-J>
nnoremap <leader>k <C-W><C-K>
nnoremap <leader>l <C-W><C-L>
nnoremap <leader>h <C-W><C-H>

"Movement
inoremap jk <Esc>
nnoremap J <C-d>
nnoremap K  <C-u>
nnoremap H ^
nnoremap L $
xnoremap J <C-d>
xnoremap K  <C-u>
xnoremap H ^
xnoremap L $
nnoremap S :w<cr>
inoremap kk <Esc>la

"(source) config
nnoremap <leader>sv :source ~/.config/nvim/init.vim<cr>
nnoremap <leader>ev :vsplit ~/.config/nvim/init.vim<cr>

" TOIlet
nmap <leader>as :.!toilet -w 200 -f "ANSI Shadow"<CR>
nmap <leader>bs :.!toilet -w 200 -f term -F border<CR>
vmap <leader>bb :'<,'>  !boxes -a hcvc <CR>

" Copy Pasta
nnoremap <C-p> "+p
nnoremap <C-c> "+y

vnoremap <C-p> "+p
vnoremap <C-c> "+y

" nmap <S-CR> O<Esc>
" nmap <CR> o<Esc>

"Highlight
nmap <silent> <esc> :noh<cr>

"terminal
tnoremap <Esc> <C-\><C-n>

"tags
nnoremap ö <C-]>
nnoremap é <C-O>

nnoremap ü ]s
nnoremap è [s

"resize 
nnoremap <silent> <Leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent> <Leader>- :exe "resize " . (winheight(0) * 2/3)<CR>

