function! Executable()
    !chmod +x %
endfunction 

nnoremap <leader>ex :call Executable()<CR>

