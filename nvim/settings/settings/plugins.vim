" Plugins Setting

"vim-auto-save
let g:auto_save = 1  "AutoSave on Vim startup

"indentLine
let g:indentLine_faster = 1
let g:indentLine_enabled = 1
let g:indentLine_char = '|'
let g:indentLine_fileTypeExclude = ['text', 'help', 'markdown', 'vimwiki', 'coc-explorer']

"ALE
let g:ale_sign_error = '>>'
let g:ale_sign_warning = '-'
nmap <silent> <C-w> <Plug>(ale_toggle)
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

"tagbar
nmap <silent> <leader>tt :TagbarToggle<CR>
let g:tagbar_compact = 1

" vim-commentary
autocmd FileType matlab setlocal commentstring=%\ %s

"vimwiki
let g:vimwiki_list = [{'path': '~/Nextcloud/Notes/dennys_notes',
                      \ 'path_html': '~/Nextcloud/Notes/dennys_notes_html',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

hi VimwikiHeader1 guifg=#ebcb8b
hi VimwikiHeader3 guifg=#b48ead
hi VimwikiHeader2 guifg=#a3be8c
hi VimwikiHeader4 guifg=#96b5b4
hi VimwikiHeader5 guifg=#bf616a
hi VimwikiHeader6 guifg=#778899

let vimwiki_auto_header = 1


set foldmethod=syntax
set nofoldenable
set foldlevel=99

set nocompatible
if has("autocmd")
    filetype plugin indent on
endif

let g:vimwiki_filetypes = ['markdown','pandoc']
let g:vimwiki_folding = 'custom'

au FileType vimwiki.markdown.pandoc setlocal shiftwidth=6 tabstop=6 noexpandtab
let g:markdown_fold_style = 'nested'

augroup pandoc_syntax
    au! BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc
augroup END


let g:pandoc#syntax#conceal#urls=1
nnoremap <leader>cc :call MyConceal()<CR>
nnoremap <leader>cl :set conceallevel=2<CR>
nnoremap <leader>co :set conceallevel=0<CR>

function! MyConceal()
    set concealcursor=""
    set conceallevel=2
    hi Conceal gui=bold guifg=#BF616A
endfunction

"vim-clang-format
let g:clang_format#style_options = {
            \ "AccessModifierOffset" : -4,
            \ "AllowShortIfStatementsOnASingleLine" : "Never",
            \ "AlignConsecutiveAssignments" : "true",
            \ "AlignConsecutiveDeclarations" : "true",
            \ "AlignConsecutiveMacros" : "true",
            \ "AllowShortFunctionsOnASingleLine" : "Empty",
            \ "Standard" : "C++11"}

" map to <Leader>cf in C++ code
autocmd FileType c,cpp,objc nnoremap <buffer><Leader>cf :<C-u>ClangFormat<CR>
autocmd FileType c,cpp,objc vnoremap <buffer><Leader>cf :ClangFormat<CR>

nmap <Leader>C :ClangFormatAutoToggle<CR>

" Rainbow 
let g:rainbow_active = 0
let g:rainbow_conf = {
\	'guifgs': ['#b48ead', '#a3be8c', '#ebcb8b', '#bf616a'],
\}

"vimtex
let g:tex_flavor = 'latex'
let g:vimtex_view_method = 'zathura'

"cpp highlighting
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1


" MD Preview
let g:mkdp_auto_close = 0
let g:mkdp_page_title = '${name}'
let g:mkdp_markdown_css = '~/.config/nvim/settings/markdown/github-markdown-css/github-markdown.css'

"Python Black (Autoformat)
nnoremap <leader>bp :Black<CR>
let b:ale_fixers = ['black']

" Command T
nmap <silent> <Leader>t <Plug>(CommandT)
nmap <silent> <Leader>b <Plug>(CommandTBuffer)
nmap <silent> <Leader>0 <Plug>(CommandTBuffer)
nmap <silent> <Leader>jj <Plug>(CommandTJump)





