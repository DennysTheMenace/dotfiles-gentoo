hi clear
syntax reset
let g:colors_name = "spaceodyssey"
set background=dark
set t_Co=256
hi Normal guifg=#e1e1e1 ctermbg=NONE gui=NONE

hi DiffText guifg=#ebcb8b guibg=NONE
hi ErrorMsg guifg=#ebcb8b guibg=NONE
hi WarningMsg guifg=#ebcb8b guibg=NONE
hi PreProc guifg=#ebcb8b guibg=NONE
hi Exception guifg=#ebcb8b guibg=NONE
hi Error guifg=#ebcb8b guibg=NONE
hi DiffDelete guifg=#ebcb8b guibg=NONE
hi GitGutterDelete guifg=#ebcb8b guibg=NONE
hi GitGutterChangeDelete guifg=#ebcb8b guibg=NONE
hi cssIdentifier guifg=#ebcb8b guibg=NONE
hi cssImportant guifg=#ebcb8b guibg=NONE
hi Type guifg=#ebcb8b guibg=NONE
hi Identifier guifg=#ebcb8b guibg=NONE
hi PMenuSel guifg=#96b5b4 guibg=NONE
hi Constant guifg=#a3be8c guibg=NONE
hi Repeat guifg=#a3be8c guibg=NONE
hi DiffAdd guifg=#a3be8c guibg=NONE
hi GitGutterAdd guifg=#a3be8c guibg=NONE
hi cssIncludeKeyword guifg=#a3be8c guibg=NONE
hi Keyword guifg=#a3be8c guibg=NONE
hi IncSearch guifg=#96b5b4 guibg=NONE
hi Title guifg=#96b5b4 guibg=NONE
hi PreCondit guifg=#96b5b4 guibg=NONE
hi Debug guifg=#96b5b4 guibg=NONE
hi SpecialChar guifg=#96b5b4 guibg=NONE
hi Conditional guifg=#96b5b4 guibg=NONE
hi Todo guifg=#96b5b4 guibg=NONE
hi Special guifg=#96b5b4 guibg=NONE
hi Label guifg=#96b5b4 guibg=NONE
hi Delimiter guifg=#96b5b4 guibg=NONE
hi Number guifg=#96b5b4 guibg=NONE
hi CursorLineNR guifg=#96b5b4 guibg=NONE
hi Define guifg=#96b5b4 guibg=NONE
hi MoreMsg guifg=#96b5b4 guibg=NONE
hi Tag guifg=#96b5b4 guibg=NONE
hi String guifg=#96b5b4 guibg=NONE
hi MatchParen guifg=#96b5b4 guibg=NONE
hi Macro guifg=#96b5b4 guibg=NONE
hi DiffChange guifg=#96b5b4 guibg=NONE
hi GitGutterChange guifg=#96b5b4 guibg=NONE
hi cssColor guifg=#96b5b4 guibg=NONE
hi Function guifg=#8fa1b3 guibg=NONE
hi Directory guifg=#b48ead guibg=NONE
hi markdownLinkText guifg=#b48ead guibg=NONE
hi javaScriptBoolean guifg=#b48ead guibg=NONE
hi Include guifg=#b48ead guibg=NONE
hi Storage guifg=#b48ead guibg=NONE
hi cssClassName guifg=#b48ead guibg=NONE
hi cssClassNameDot guifg=#b48ead guibg=NONE
hi Statement guifg=#bf616a guibg=NONE
hi Operator guifg=#bf616a guibg=NONE
hi cssAttr guifg=#bf616a guibg=NONE

hi Folded guibg=#1c2023 guifg=#96b5b4

hi Pmenu guifg=#e1e1e1 guibg=#1c2023
hi SignColumn guibg=#1c2023
hi Title guifg=#96b5b4
hi LineNr guifg=#7b7b7b
hi NonText guifg=#919ba0
hi Comment guifg=#919ba0 gui=italic
hi SpecialComment guifg=#919ba0 gui=italic guibg=#1c2023
hi CursorLine guibg=#090909
" hi TabLineFill gui=NONE guibg=#090909
" hi TabLine guifg=#7b7b7b guibg=#090909 gui=NONE
hi StatusLine gui=bold  guifg=#e1e1e1
hi StatusLineNC gui=NONE guifg=#e1e1e1
hi Search guibg=#bf616a guifg=#1c2023
hi VertSplit gui=NONE guifg=#090909 guibg=NONE
hi Visual gui=NONE guibg=#090909


let g:terminal_color_0  = '#1c2023'
let g:terminal_color_1  = '#bf616a'
let g:terminal_color_2  = '#a3be8c'
let g:terminal_color_3  = '#ebcb8b'
let g:terminal_color_4  = '#96b5b4'
let g:terminal_color_5  = '#b48ead'
let g:terminal_color_6  = '#96b5b4'
let g:terminal_color_7  = '#e1e1e1'
let g:terminal_color_8  = '#919ba0'
let g:terminal_color_9  = '#bf616a'
let g:terminal_color_10 = '#a3be8c'
let g:terminal_color_11 = '#ebcb8b'
let g:terminal_color_12 = '#96b5b4'
let g:terminal_color_13 = '#b48ead'
let g:terminal_color_14 = '#96b5b4'
let g:terminal_color_15 = '#919ba0'
