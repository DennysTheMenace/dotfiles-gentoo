"lightline
set laststatus=2 

" Coc-Support
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

"Devicons
let g:webdevicons_enable = 1

let g:lightline = {
   \ 'colorscheme': 'space',
   \ 'active': {
   \   'left': [ ['mode', 'paste'],
   \             ['cocstatus', 'fugitive', 'readonly', 'filename', 'modified'] ],
   \   'right': [ ['file'], ['fileformat'], [ 'lineinfo' ], ['buffernum'],
   \            [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_infos', 'linter_ok' ] ]
   \ },
   \ 'component': {
   \   'readonly': '%{&filetype=="help"?"":&readonly?"∢":""}',
   \   'modified': '%{&filetype=="help"?"":&modified?"+":&modifiable?"":"-"}',
   \ },
   \ 'component_visible_condition': {
   \   'readonly': '(&filetype!="help"&& &readonly)',
   \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))',
   \ },
   \ 'separator': { 'left': ' ', 'right': ' ' },
   \ 'subseparator': { 'left': ' ', 'right': ' ' },
   \ 'component_function': {
	 \   'cocstatus': 'coc#status',
   \   'file': 'MyFiletype',
   \   'fileformat': 'MyFileformat',
   \   'buffernum': 'BufferNum',
   \   'fugitive': 'LightlineFugitive',
	 \ },
   \'component_expand': {
   \  'linter_checking': 'lightline#ale#checking',
   \  'linter_infos': 'lightline#ale#infos',
   \  'linter_warnings': 'lightline#ale#warnings',
   \  'linter_errors': 'lightline#ale#errors',
   \  'linter_ok': 'lightline#ale#ok',
   \ },
   \'component_type': {
   \  'linter_checking': 'right',
   \  'linter_infos': 'right',
   \  'linter_warnings': 'warning',
   \  'linter_errors': 'error',
   \  'linter_ok': 'right',
   \ },
\ }
 
let g:lightline#ale#indicator_checking = "⧗"
let g:lightline#ale#indicator_infos = "!"
let g:lightline#ale#indicator_warnings = "?"
let g:lightline#ale#indicator_errors = "✗"
let g:lightline#ale#indicator_ok = "✓"

autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()
autocmd BufWritePost,TextChanged,TextChangedI * call lightline#update()

function! MyFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
endfunction

function! MyFileformat()
  return winwidth(0) > 70 ? (&fileformat . ' ' . WebDevIconsGetFileFormatSymbol()) : ''
endfunction

function! BufferNum()
  return bufnr('%')
endfunction

function! s:update_bufnrs() abort
  let s:buffer_count_by_basename = {}
  let bufnrs = filter(range(1, bufnr('$')), 'buflisted(v:val) && bufexists(v:val) && len(bufname(v:val))')
  for name in map(bufnrs, 'expand("#" . v:val . ":t")')
    if name !=# ''
      let s:buffer_count_by_basename[name] = get(s:buffer_count_by_basename, name) + 1
    endif
  endfor
endfunction

function! LightlineFugitive() abort
  if &filetype ==# 'help'
    return ''
  endif
  if has_key(b:, 'lightline_fugitive') && reltimestr(reltime(b:lightline_fugitive_)) =~# '^\s*0\.[0-5]'
    return b:lightline_fugitive
  endif
  try
    if exists('*fugitive#head')
      let head = fugitive#head()
    else
      return ''
    endif
    let b:lightline_fugitive = head
    let b:lightline_fugitive_ = reltime()
    return b:lightline_fugitive
  catch
  endtry
  return ''
endfunction
