
"███╗   ██╗██╗   ██╗██╗███╗   ███╗
"████╗  ██║██║   ██║██║████╗ ████║
"██╔██╗ ██║██║   ██║██║██╔████╔██║
"██║╚██╗██║╚██╗ ██╔╝██║██║╚██╔╝██║
"██║ ╚████║ ╚████╔╝ ██║██║ ╚═╝ ██║
"╚═╝  ╚═══╝  ╚═══╝  ╚═╝╚═╝     ╚═╝

" Plugins

call plug#begin()

" Tpope 
Plug 'tpope/vim-surround'

Plug 'tpope/vim-eunuch'

Plug 'tpope/vim-fugitive'

Plug 'tpope/vim-commentary'

Plug 'tpope/vim-repeat'


"Coding

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'dense-analysis/ale'

Plug 'maximbaz/lightline-ale'

Plug '907th/vim-auto-save'

Plug 'honza/vim-snippets'


Plug 'rhysd/vim-clang-format'

Plug 'Yggdroot/indentLine'

Plug 'majutsushi/tagbar'

Plug 'jackguo380/vim-lsp-cxx-highlight'

Plug 'lervag/vimtex'

" Plug 'psf/black', { 'branch': 'stable' }


" Tags

Plug 'ludovicchabant/vim-gutentags'


" Text Objects

Plug 'kana/vim-textobj-user'

Plug 'libclang-vim/vim-textobj-clang'

Plug 'coachshea/vim-textobj-markdown'

Plug 'bps/vim-textobj-python'

Plug 'kana/vim-textobj-function'

Plug 'kana/vim-textobj-indent'

Plug 'kana/vim-textobj-entire'


" Notes and Essay

Plug 'sheerun/vim-polyglot'

Plug 'vimwiki/vimwiki'

Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

Plug 'vim-pandoc/vim-pandoc-syntax'

Plug 'masukomi/vim-markdown-folding'

Plug 'KeitaNakamura/tex-conceal.vim', {'for': ['tex', 'markdown']}


" Look and Feel


Plug 'itchyny/lightline.vim'

Plug 'ryanoasis/vim-devicons'

Plug 'luochen1990/rainbow'

" Tools

Plug 'DanilaMihailov/vim-tips-wiki'

Plug 'wincent/command-t', {'do': 'cd ruby/command-t/ext/command-t && ruby extconf.rb && make'}


call plug#end()           


" Settings

source ~/.config/nvim/settings/general.vim
source ~/.config/nvim/settings/keys.vim
source ~/.config/nvim/settings/colorscheme.vim
source ~/.config/nvim/settings/spaceodyssey.vim
source ~/.config/nvim/settings/statusbar.vim
source ~/.config/nvim/settings/plugins.vim
source ~/.config/nvim/settings/coc-settings.vim
source ~/.config/nvim/settings/functions.vim
